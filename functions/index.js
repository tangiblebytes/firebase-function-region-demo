const functions = require("firebase-functions");

exports.helloEnv = functions
  .region("europe-west2")
  .https.onRequest((request, response) => {
    response.send(process.env);
  });
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions
  .region("europe-west2")
  .https.onRequest((request, response) => {
    let origin = process.env["FUNCTIONS_EMULATOR"]
      ? "http://localhost:5002"
      : "https://function-region-demo.web.app";
    response.set("Access-Control-Allow-Origin", origin);

    if (request.method === "OPTIONS") {
      response.set({
        "Access-Control-Allow-Methods": "GET",
        "Access-Control-Allow-Headers": "Content-Type",
        "Access-Control-Max-Age": "3600",
      });
      // Send response to OPTIONS requests
      response.status(204).send("");
    } else {
      response.send("Hello from europe-west2 https Function!");
    }
  });

exports.helloCallable = functions
  .region("europe-west2")
  .https.onCall((data, context) => {
    return "Hello from europe-west2 Callable";
  });
