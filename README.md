# firebase-function-region-demo

I had trouble getting my firebase functions working in my local region

So I made a simple project to test it

and sure enough it works fine

Indeed there isn't much to it

index.html contains the clientside code and functions/index.js the serverside

If you want to make ajax requests to an http function it seems you need to set the CORS headers

But the callable function works without any specific setup.

Demo is live at https://function-region-demo.web.app/
